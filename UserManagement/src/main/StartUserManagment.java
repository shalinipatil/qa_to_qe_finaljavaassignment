package main;

import java.io.IOException;

import operations.PerformAction;

public class StartUserManagment {

	
	
	public static void main(String[] args) throws NumberFormatException, IOException {
		System.out.println("=======================================================");
		System.out.println("\n==============  Welcome To User Management ============\n");
		System.out.println("=======================================================");
		PerformAction O = new PerformAction();
		
		//O.options();
		O.operationTobePerformed();
		O.performingHere();
		
	}

}
