package user;

import java.sql.Date;

public class UserIdentity {

	int Id, Age;
	String Name, EmailId, Address;
	Date DateOfBirth, CreateDate, UpdateDate;

	public UserIdentity(){
	}

	public UserIdentity(int id, int age, String name, String emailId, String address, Date dateOfBirth, Date createDate,
			Date updateDate) {
		super();
		Id = id;
		Age = age;
		Name = name;
		EmailId = emailId;
		Address = address;
		DateOfBirth = dateOfBirth;
		CreateDate = createDate;
		UpdateDate = updateDate;
	}

	public int getId() {
		return Id;
	}

	public void setId(int id) {
		Id = id;
	}

	public int getAge() {
		return Age;
	}

	public void setAge(int age) {
		Age = age;
	}

	public String getName() {
		return Name;
	}

	public void setName(String name) {
		Name = name;
	}

	public String getEmailId() {
		return EmailId;
	}

	public void setEmailId(String emailId) {
		EmailId = emailId;
	}

	public String getAddress() {
		return Address;
	}

	public void setAddress(String address) {
		Address = address;
	}

	public Date getDateOfBirth() {
		return DateOfBirth;
	}

	public void setDateOfBirth(Date dateOfBirth) {
		DateOfBirth = dateOfBirth;
	}

	public Date getCreateDate() {
		return CreateDate;
	}

	public void setCreateDate(Date createDate) {
		CreateDate = createDate;
	}

	public Date getUpdateDate() {
		return UpdateDate;
	}

	public void setUpdateDate(Date updateDate) {
		UpdateDate = updateDate;
	}
	
	
	
	
	
	
	
	

	/*@Override
	public int compareTo(User u1) {
		// TODO Auto-generated method stub
		return this.Name.compareTo(u1.getName());;
	}
	public int compareTo(User u2) {
		// TODO Auto-generated method stub
		return this.Name.compareTo(u1.getName());;
	}*/
	
	
	 /*public static Comparator<User> userName = new Comparator<User>() {

			public int compare(User s1, User s2) {
			   String UserName1 = s1.getName().toUpperCase();
			   String UserName2 = s2.getName().toUpperCase();

			   //ascending order
			   return UserName1.compareTo(UserName2);

			   //descending order
			  // return StudentName2.compareTo(StudentName1);
		    }};

	 
		
		    public static Comparator<User> userAddress = new Comparator<User>() {

		    	public int compare(User s1, User s2) {

		    	   String address1 = s1.getAddress().toUpperCase();
		    	   String address2 = s2.getAddress().toUpperCase();

		    	   For ascending order
		    	   return address1.compareTo(address2);

		    	   For descending order
		    	   //rollno2-rollno1;
		       }};
		       
		       public static Comparator<User> userAge = new Comparator<User>() {

			    	public int compare(User s1, User s2) {

			    	   int age1 = s1.getAge();
			    	   int age2 = s2.getAge();

			    	   For ascending order
			    	   return age1-age2;

			    	   For descending order
			    	   //rollno2-rollno1;
			       }};
			       public String toString() {
			           return "[ Id=" + Id + ",Age"+Age+",Name=" + Name + ", EmailId ="+EmailId +",Addrees"+Address+",DOB"+DateOfBirth+",Created date"+CreateDate+",Updated date"+UpdateDate+"]";
			       }*/
			      
}
