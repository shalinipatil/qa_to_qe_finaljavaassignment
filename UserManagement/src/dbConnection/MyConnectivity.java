package dbConnection;

import java.sql.Connection;
import java.sql.DriverManager;

public class MyConnectivity {

	//static String jdbcDriver = "com.mysql.jdbc.Driver";
	static String ConnectionURL = "jdbc:mysql://localhost:3306/shalini";
	static String username = "root";
	static String password = "root";
	static Connection C;

	public static Connection getMyConnection() {
		try {
			//Class.forName(jdbcDriver);
			C = DriverManager.getConnection(ConnectionURL, username, password);
			return C;
		} catch (Exception e) {
			System.err.println("problem while connection establishment..." + e);
			return C;
		}

	}

}
