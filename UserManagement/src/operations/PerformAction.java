package operations;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.InputMismatchException;
import java.util.Set;
import java.util.TreeMap;

import dbConnection.MyConnectivity;

//import org.omg.PortableInterceptor.ORBInitInfoPackage.DuplicateName;

//import myexception.DuplicateName;
import myexception.OverAgeException;
import user.UserIdentity;

public class PerformAction {

	Connection connectdb;
	BufferedReader readinput = new BufferedReader(new InputStreamReader(System.in));
	int choice;

	public void operationTobePerformed() {

		System.out.println("\t\tAvailable Operations = >");
		System.out.println("\t\t1.Show Existing Users");
		System.out.println("\t\t2.Add new User");
		System.out.println("\t\t3.Update existing User");
		System.out.println("\t\t4.Delete Existing User");
		System.out.println("\t\t5.Sort User");
		System.out.println("\t\t6.Search User");
		System.out.println("\t\t7.Exit");
		System.out.println(
				"===============================================================================================================================================");

	}

	public void performingHere() throws NumberFormatException, IOException {

		try {

			do {

				System.out.println("\nEnter your choice=: ");
				choice = Integer.parseInt(readinput.readLine());
				switch (choice) {
				case 1:
					fetchUsersList();
					break;
				case 2:
					addUser();
					break;
				case 3:
					updateUser();
					break;
				case 4:
					deleteUser();
					break;
				case 5:
					sortUsers();
					break;
				case 6:
					searchUsers();
					break;
				case 7:
					System.out.println("Thank you.........");
					//readinput.close();
					break;
				default:
					System.err.println("Invalid  input for choice (1 - 7)\n" + "Try again\n");
					repeatOperations();
					break;

				}

			} while (choice != 7);

		} catch (NumberFormatException e) {
			{
				System.err.println("Please Enter digit only.");
				repeatOperations();

			}
		} catch (InputMismatchException e) {
			System.err.println("Please Enter valid input.");
			repeatOperations();
		} catch (Exception e) {
			System.out.println(e);
			// this.repeatMenue();
		}

	}

	public void addUser() throws NumberFormatException, IOException {
		try {

			System.out.println("Please Enter following details of user :");
			System.out.println("Name :");
			String Name = readinput.readLine();
			System.out.println("Email :");
			String Email = readinput.readLine();
			System.out.println("Address :");
			String Address = readinput.readLine();
			System.out.println("Age :");
			int age = Integer.parseInt(readinput.readLine());
			System.out.println("Date of Birth (yyyy-mm-dd) :");
			String d = readinput.readLine();
			Date date = Date.valueOf(d);

			if (age > 60) {
				throw new OverAgeException();
			}

			UserIdentity U = new UserIdentity(1, age, Name, Email, Address, date, new Date(System.currentTimeMillis()),
					new Date(System.currentTimeMillis()));

			connectdb = MyConnectivity.getMyConnection();
			PreparedStatement ps1 = null;
			ResultSet rs1 = null;

			String query = "Select * from userdetails where name = ?";
			ps1 = connectdb.prepareStatement(query);
			ps1.setString(1, Name);
			rs1 = ps1.executeQuery();

			int size = 0;
			if (rs1 != null) {
				rs1.last(); // moves cursor to the last row
				size = rs1.getRow(); // get row id
			}
			if (size > 0) {

				System.err.println("User with this name already exists");
				
			} 
			else {

				PreparedStatement ps = connectdb.prepareStatement(
						"insert into userdetails(id,name,email,address,age,dob,createdate,updatedate) values(?,?,?,?,?,?,?,?)");
				PreparedStatement formax = connectdb.prepareStatement("select max(id) from userdetails");
				ResultSet rs = formax.executeQuery();
				int max = 0;
				while (rs.next()) {
					max = rs.getInt(1);
				}
				if (max != 0) {
					ps.setInt(1, max + 1);
				} else {
					ps.setInt(1, 1);
				}

				ps.setString(2, U.getName());
				ps.setString(3, U.getEmailId());
				ps.setString(4, U.getAddress());
				ps.setInt(5, U.getAge());
				ps.setString(6, U.getDateOfBirth().toString());
				ps.setString(7, U.getCreateDate().toString());
				ps.setString(8, U.getUpdateDate().toString());
				ps.executeUpdate();
				System.out.println("user added successfully...");
				connectdb.close();
				
			}
			
			repeatOperations();
			

		} catch (OverAgeException oa) {
			System.err.println("try again\n");
			repeatOperations();
		} catch (InputMismatchException e) {
			System.err.println("Please Enter Input properly");
			System.err.println("try again\n");
			repeatOperations();
		} catch (Exception e) {

			System.err.println("Entered details are incorrect. Please enter correct details");
			repeatOperations();

		}

	}

	public void sortUsers() throws NumberFormatException, IOException {

		System.out.println("Select field for sorting:");
		System.out.println("1.Name");
		System.out.println("2.Address");
		System.out.println("3.Age");

		try {
			int ch = Integer.parseInt(readinput.readLine());

			if (ch == 1) {
				sortUser("name");
			} else if (ch == 2) {
				sortUser("address");
			} else {
				sortUser("age");
			}

			repeatOperations();

		} catch (NumberFormatException e) {

			e.printStackTrace();
			repeatOperations();

		} catch (IOException e) {

			e.printStackTrace();
			repeatOperations();

		}
	}
	
	public void sortUser(String sortPara) throws NumberFormatException, IOException {
		try {
			ArrayList<UserIdentity> al2 = new ArrayList<UserIdentity>();
			System.out.println("Existing Users :\n");
			TreeMap<String, UserIdentity> sortedMap = new TreeMap<String, UserIdentity>();
			TreeMap<Integer, UserIdentity> sortedMapAge = new TreeMap<Integer, UserIdentity>();
			connectdb = MyConnectivity.getMyConnection();
			PreparedStatement ps = connectdb.prepareStatement("select * from userdetails");
			ResultSet rs = ps.executeQuery();

			if (rs.next()) {
				rs = ps.executeQuery();

				while (rs.next()) {
					UserIdentity user = new UserIdentity();
					user.setId(rs.getInt(1));
					user.setName(rs.getString(2));
					user.setEmailId(rs.getString(3));
					user.setAddress(rs.getString(4));
					user.setAge(rs.getInt(5));
					user.setDateOfBirth(Date.valueOf(rs.getString(6)));
					user.setCreateDate(Date.valueOf(rs.getString(7)));
					user.setUpdateDate(Date.valueOf(rs.getString(8)));
					sortPara.trim();
					if (sortPara.equalsIgnoreCase("Name"))
						//
						sortedMap.put(user.getName(), user);
					else if (sortPara.equalsIgnoreCase("Age"))
						sortedMapAge.put(user.getAge(), user);
					else if (sortPara.equalsIgnoreCase("Address"))
						sortedMap.put(user.getAddress(), user);
				}
				if (sortPara.equalsIgnoreCase("Name")) {
					Set<String> keySet = sortedMap.keySet();
					for (String key : keySet) {
						al2.add(sortedMap.get(key));
					}
					showUsers(al2);
					repeatOperations();
				} else if (sortPara.equalsIgnoreCase("Age")) {
					Set<Integer> keySet = sortedMapAge.keySet();
					for (Integer key : keySet) {
						al2.add(sortedMapAge.get(key));
					}
					showUsers(al2);
					repeatOperations();
				} else if (sortPara.equalsIgnoreCase("Address")) {
					Set<String> keySet = sortedMap.keySet();
					for (String key : keySet) {
						al2.add(sortedMap.get(key));
					}
					showUsers(al2);
					repeatOperations();
				}
				connectdb.close();

			} else {
				System.err.println("There is no user");

			}
		} catch (Exception e) {
			System.out.println(e);
			repeatOperations();

		}
	}


	public void searchUsers() throws NumberFormatException, IOException {

		System.out.println("Enter Emailid or Name for search: ");
		try {
			String str = readinput.readLine();
			searchUser(str);

		} catch (Exception e) {
			System.out.println("Incorrect name or Email id");
			repeatOperations();
		}

	}

	
	public void searchUser(String searchString) throws NumberFormatException, IOException {
		try {
			System.out.println("Existing Users :\n");
			PreparedStatement ps = null;
			connectdb = MyConnectivity.getMyConnection();
			if (searchString.contains("@"))
				ps = connectdb.prepareStatement("select * from userdetails where email=?");
			else
				ps = connectdb.prepareStatement("select * from userdetails where name=?");
			String s =searchString.trim();
			ps.setString(1, s);
			ResultSet rs = ps.executeQuery();
			if (rs.next()) {
				rs = ps.executeQuery();
				ArrayList<UserIdentity> AL = new ArrayList<UserIdentity>();

				while (rs.next()) {
					UserIdentity user = new UserIdentity();
					user.setId(rs.getInt(1));
					user.setName(rs.getString(2));
					user.setEmailId(rs.getString(3));
					user.setAddress(rs.getString(4));
					user.setAge(rs.getInt(5));
					user.setDateOfBirth(Date.valueOf(rs.getString(6)));
					user.setCreateDate(Date.valueOf(rs.getString(7)));
					user.setUpdateDate(Date.valueOf(rs.getString(8)));
					AL.add(user);
				}
				showUsers(AL);
				repeatOperations();

				connectdb.close();

			} else {
				System.err.println("There are no users");
				repeatOperations();

			}
		} catch (Exception e) {
			System.out.println(e);
			repeatOperations();

		}
	}

	public void updateUser() throws NumberFormatException, IOException {
		try {

			getUserData();
			System.out.println("Enter ID to Update :");
			int id = Integer.parseInt(readinput.readLine());
			System.out.println("Select field to be updated:");
			System.out.println("1.Name");
			System.out.println("2.Email");
			System.out.println("3.Address");
			System.out.println("4.Age");
			System.out.println("5.DOB");
			int ch = Integer.parseInt(readinput.readLine());

			if (ch == 5) {
				System.out.println("Enter new value of date (yyyy-mm-dd):");
			} else {
				System.out.println("Enter new value :");
			}
			String newValue = readinput.readLine();
			if (ch == 4 && Integer.parseInt(newValue) > 60) {
				throw new OverAgeException();
			}
			connectdb = MyConnectivity.getMyConnection();
			String column = "";

			switch (ch) {
			case 1:
				column = "name";
				break;
			case 2:
				column = "email";
				break;
			case 3:
				column = "Address";
				break;
			case 4:
				column = "age";
				break;
			case 5:
				column = "DOB";
				break;
			}
			PreparedStatement ps = connectdb
					.prepareStatement("update userdetails set " + column + "='" + newValue + "' where ID=" + id);
			int r = ps.executeUpdate();
			if (r == 0) {
				System.out.println("no record updated");
			} else {
				System.out.println(r + " records updated successfully..");
			}

			connectdb.close();
			repeatOperations();

		} catch (OverAgeException oa) {
			System.err.println("try again\n");
			repeatOperations();
		} catch (InputMismatchException e) {
			System.err.println("Please Enter Input properly");
			System.err.println("try again\n");
			repeatOperations();
		} catch (Exception e) {
			{
				System.err.println("Please Enter date in 'yyyy-mm-dd' format" + e);
				repeatOperations();
			}

			System.err.println("Incorrect details. Please check details\n");
			repeatOperations();
		}

	}

	public void deleteUser() throws NumberFormatException, IOException {
		try {

			getUserData();
			System.out.println("\n Enter ID of User to Delete :");
			int d = Integer.parseInt(readinput.readLine());
			connectdb = MyConnectivity.getMyConnection();
			PreparedStatement ps = connectdb.prepareStatement("delete from userdetails where Id=?");
			ps.setInt(1, d);
			int r = ps.executeUpdate();
			if (r == 0) {
				System.out.println("No such record found");
			} else {
				System.out.println("Record deleted successfuly");
			}
			repeatOperations();
		} catch (Exception e) {
			System.out.println(e);
			repeatOperations();
		}

	}

	public void showUsers(ArrayList<UserIdentity> AL) {
		// ArrayList<User> AL = this.fetchUsersList();
		System.out.println(
				"===============================================================================================================================================");

		System.out.println("ID\t|Name\t\tEmail\t\tAddress\t\tAge\t\tDate Of Birth\t\tCreate Date\t\tUpdate Date");
		System.out.println(
				"-----------------------------------------------------------------------------------------------------------------------------------------------");

		for (UserIdentity user : AL) {
			System.out.println(user.getId() + "\t" + user.getName() + "\t\t" + user.getEmailId() + "\t"
					+ user.getAddress() + "\t\t" + user.getAge() + "\t\t" + user.getDateOfBirth() + "\t\t"
					+ user.getCreateDate() + "\t\t" + user.getUpdateDate());
		}
		System.out.println(
				"===============================================================================================================================================");

	}

	public void fetchUsersList() {
		try {
			ArrayList<UserIdentity> al1 = new ArrayList<UserIdentity>();
			System.out.println("Existing Users :\n");
			connectdb = MyConnectivity.getMyConnection();
			PreparedStatement ps = connectdb.prepareStatement("select * from userdetails");
			ResultSet rs = ps.executeQuery();

			if (rs.next()) {
				rs = ps.executeQuery();
				while (rs.next()) {
					UserIdentity user = new UserIdentity();
					user.setId(rs.getInt(1));
					user.setName(rs.getString(2));
					user.setEmailId(rs.getString(3));
					user.setAddress(rs.getString(4));
					user.setAge(rs.getInt(5));
					user.setDateOfBirth(Date.valueOf(rs.getString(6)));
					user.setCreateDate(Date.valueOf(rs.getString(7)));
					user.setUpdateDate(Date.valueOf(rs.getString(8)));
					al1.add(user);

				}

				connectdb.close();
				showUsers(al1);
				//rs = null;

				// return AL;
			} else {
				System.err.println("There are no users");
				// return null;
			}
			repeatOperations();
		} catch (Exception e) {
			System.out.println(e);
			// return null;
		}

	}

	public void getUserData() {
		try {
			ArrayList<UserIdentity> al1 = new ArrayList<UserIdentity>();
			System.out.println("Existing Users :\n");
			connectdb = MyConnectivity.getMyConnection();
			PreparedStatement ps = connectdb.prepareStatement("select * from userdetails");
			ResultSet rs = ps.executeQuery();

			if (rs.next()) {
				rs = ps.executeQuery();
				// ArrayList<User> AL = new ArrayList<User>();

				while (rs.next()) {
					UserIdentity user = new UserIdentity();
					user.setId(rs.getInt(1));
					user.setName(rs.getString(2));
					user.setEmailId(rs.getString(3));
					user.setAddress(rs.getString(4));
					user.setAge(rs.getInt(5));
					user.setDateOfBirth(Date.valueOf(rs.getString(6)));
					user.setCreateDate(Date.valueOf(rs.getString(7)));
					user.setUpdateDate(Date.valueOf(rs.getString(8)));
					al1.add(user);

				}

				connectdb.close();
				showUsers(al1);
				// rs=null;

				// return AL;
			} else {
				System.err.println("There are no users");
				// return null;
			}

		} catch (Exception e) {
			System.out.println(e);
			// return null;
		}

	}

	public void repeatOperations() throws NumberFormatException, IOException {
		System.out.println("Please press 5 for menue");
		int key = Integer.parseInt(readinput.readLine());
		if (key == 5) {
			operationTobePerformed();
			performingHere();
		} else {
			System.err.println("Wrong input please press 5");
			repeatOperations();
		}

	}
}
