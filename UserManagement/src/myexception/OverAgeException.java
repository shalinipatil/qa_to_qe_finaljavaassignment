package myexception;

public class OverAgeException extends Exception {

	//private static final long serialVersionUID = 1L;

	public OverAgeException() {
		System.err.println("Age of User must be less than 60");
	}

}
